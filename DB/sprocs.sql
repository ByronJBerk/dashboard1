DELIMITER //

DROP PROCEDURE IF EXISTS spGetDailyPrices //
CREATE PROCEDURE spGetDailyPrices(IN start_date DATE, IN end_date DATE)

   SELECT s.ticker AS Ticker, p.price_date AS Date, p.open_price AS Open, p.high_price AS High, p.low_price AS Low,
          p.close_price AS Close, p.volume AS Volume, p.adj_close_price AS 'Adj Close'
       FROM daily_price AS p
       INNER JOIN symbol AS s ON p.symbol_id = s.id
       WHERE p.price_date between start_date and end_date
       ORDER BY s.ticker, p.price_date ASC;
//

DELIMITER ;
