import numpy as np
import pandas as pd
import pandas_datareader.data as pdr
import datetime
import statsmodels.api as sm
import urllib
import ConfigParser
from bs4 import BeautifulSoup
import MySQLdb as mdb
import pandas.io.sql as psql

"""
    Generates a table of statistics upon a set of ticker symbols, for example
    the S&P 500. Stack ranks symbols according to annualized, adjusted exponential
    regression slope in descending order. Provides indication of whether to trade
    based upon strategy outlined in Stocks On The Move book.
"""
def read_config():

    Config = ConfigParser.ConfigParser()
    Config.read("dashboard.config")
    print (Config.sections())
    return Config

def config_section_map(Config, section):
    dict1 = {}
    options = Config.options(section)
    for option in options:
        try:
            dict1[option] = Config.get(section, option)
            if dict1[option] == -1:
                print("skip: %s" % option)
        except:
            print("exception on %s!" % option)
            dict1[option] = None
    return dict1


def get_wiki_sp500_tickers():
    """
    scrape current S&P500 ticker symbols from the web and return list
    """
    symbols = []
    html = urllib.urlopen('https://en.wikipedia.org/wiki/List_of_S%26P_500_companies')

    page = html.read()
    soup = BeautifulSoup(page, "lxml")

    table = soup.find_all('table')[0]
    rows = table.find_all('tr')

    for tr in rows:
        tds = tr.find_all('td')
        for td in tds[0:1]:
            symbols.append(tds[0].get_text().encode("UTF-8"))

    return symbols

def get_yahoo_prices(ticker, start, end):
    def data(ticker):
        print ("Ticker: " + ticker)
	try:
	    if ticker == "UA-C":
		return pdr.DataReader("UA.C", 'yahoo', start, end)
	    else:
		return pdr.DataReader(ticker, 'yahoo', start, end)
	except:
	    print("exception on ticker " + ticker)
    datas = map(data, tickers)
    return pd.concat(datas, keys=tickers, names=['Ticker', 'Date'])

def get_db_sp500_prices(start, end, con):
    """
        Get all prices data for S&P symbols between start and end
        Ticker, Date, Open, High, Low, Close, Volume, Ad Close
    """
    sql = 'call spGetDailyPrices("' + str(start) + '", "' + str(end) + '")'
    return psql.read_sql(sql, con=con, index_col=['Ticker', 'Date'])

def print_results(output, file):
    """
    print symbols table in sorted desc order
    sort list and print out
    """
    print ("Printing output to " + file)
    output.to_csv(file)

if __name__ == "__main__":

    # get config data
    print ("Getting configuration data...")
    cfg = read_config()
    start = datetime.datetime.strptime(config_section_map(cfg, "Parameters")['start_date'], '%m/%d/%Y')
    end = datetime.datetime.strptime(config_section_map(cfg, "Parameters")['end_date'], '%m/%d/%Y')
    risk_factor = float(config_section_map(cfg, "Parameters")['risk_factor'])
    regression_period = int(config_section_map(cfg, "Parameters")['regression_period'])
    n = int(config_section_map(cfg, "Parameters")['atr_period'])
    account_value = float(config_section_map(cfg, "Parameters")['account_value'])
    output_file = config_section_map(cfg, "Parameters")['output_file']
    use_db = config_section_map(cfg,"Database")['use_database']
    db_host = config_section_map(cfg,"Database")['db_host']
    db_user = config_section_map(cfg,"Database")['db_user']
    db_pass = config_section_map(cfg,"Database")['db_pass']
    db_name = config_section_map(cfg,"Database")['db_name']

    if (use_db == "1"):
        print ("Getting price data from DB...")
        # Connect to the MySQL instance
        con = mdb.connect(db_host, db_user, db_pass, db_name)
        all_data = get_db_sp500_prices(start, end, con)

    else:
        print ("Getting S&P500 symbols from Wiki...")
        tickers = get_wiki_sp500_tickers()

        print ("Getting price data from Yahoo...")
        all_data = get_yahoo_prices(tickers, start, end)

    print ("Calculating indicators...")
    # calculate SMA200 for sp_500
    sp_500 = pdr.DataReader("^GSPC", "yahoo", start, end)
    SMA200 = np.mean(sp_500['Adj Close'][-200:])
    strRight = sp_500['Adj Close'][-1] > SMA200

    # calculate daily % move for all tickers data
    all_data["Pct_Change"] = all_data["Adj Close"].pct_change()
    all_data["Pct_Change"].fillna(0, inplace=True)

    # capture max daily % move for all tickers data
    just_pct_change = all_data[["Pct_Change"]].reset_index()

    daily_pct_cx = just_pct_change.pivot("Date", "Ticker", "Pct_Change")
    # Create/Add to output
    max_pct_change = pd.DataFrame(np.max(daily_pct_cx[-90:]), columns=['MaxPctChg'])

    # calculate SMA100 for last day of all tickers
    just_closing_prices = all_data[['Adj Close']].reset_index()
    daily_close_px = just_closing_prices.pivot('Date', 'Ticker','Adj Close')

    # add to DashboardOutput
    sma_prices = pd.DataFrame(np.mean(daily_close_px[-100:]), columns=['SMA100'])
    adj_close_prices = pd.DataFrame(np.max(daily_close_px[-1:]), columns=['Adj Close'])

    frames = (adj_close_prices, sma_prices, max_pct_change)
    output = pd.concat(frames, axis=1)
    output['ATR'] = ""      # Empty column for ATR

    tr_temp = all_data.copy()
    # drop unnecessary columns (there must be a better way to copy only desired columns)
    tr_temp.drop('Open', axis=1, inplace=True)
    tr_temp.drop('Adj Close', axis=1, inplace=True)
    tr_temp.drop('Volume', axis=1, inplace=True)

    # pivot tickers and high/low/close
    tr_temp.reset_index(inplace=True)

    #Collect unique tickers from dataframe
    ticker_list = pd.unique(tr_temp.Ticker.ravel())

    tr_temp = tr_temp.pivot('Date','Ticker')
    tr_temp = tr_temp[-21:]

    # calculate slope
    y = daily_close_px.iloc[-1*regression_period:].reset_index()  # gives entire matrix of adj close dates for symbols for last 90 days
    x = np.arange(0, regression_period)
    x = sm.add_constant(x)

    print ("Calculating linear regression values...")
    # add TR | Symbols columns to DF
    for s in ticker_list:
        true_range = pd.DataFrame( {"h-l":tr_temp['High',s] - tr_temp['Low', s],
                                   "h-cp":np.abs( tr_temp['High',s] - tr_temp['Close',s].shift(1)),
                                    "l-cp":np.abs( tr_temp['Low',s] - tr_temp['Close',s].shift(1))}, index=tr_temp.index)
        true_range = pd.Series( np.max( true_range.values, axis=1), index=tr_temp.index)
        # set TR columns
        tr_temp['TR', s] = true_range
        # n-period EMA makes this average true range (ATR)
        ATR = true_range.ewm(ignore_na=False, span=n, min_periods=n, adjust=True).mean()
	output.loc[s, 'ATR'] = ATR[-1:][0]
        # Linear regression
        output.loc[s, 'Slope'] = sm.OLS(np.log(y[s]), x).fit().params.x1
        output.loc[s, 'RSquare'] = sm.OLS(np.log(y[s]), x).fit().rsquared

    # Annual Slope and Adjusted Slope
    output['AnnualSlope'] = np.power(1+output['Slope'], 250) - 1
    output['AdjSlope'] = np.multiply(output['AnnualSlope'], output['RSquare'])

    # Calculate Trend (SMA100 > AdjClose)
    output['Trend'] = np.greater(output['Adj Close'], output['SMA100'])

    # Calculate Trailing Stop (AdjClose = 2.5*ATR)
    output['TrailingStop'] = np.subtract(output['Adj Close'], np.multiply(2.5, output['ATR']))

    # Calculate shares, Target% for all tickers
    output['NumShares'] = np.floor_divide(np.multiply(account_value, risk_factor),output['ATR'])
    output['RiskParityWeight'] = np.divide(np.multiply(output['NumShares'], output['Adj Close']), account_value)
    output['SPClose > SMA200'] = strRight

    # sort output by Trend, AdjSlope DESC
    output.sort_values(by='AdjSlope', ascending=False, inplace=True)

    print_results(output, output_file)
