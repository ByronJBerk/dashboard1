# Dashboard1
Implementation of the momentum following trading strategy outlined in the book, Stocks on the Move.

1x / week:
* Market Filter: ready to go long if ^GSPC > SMA200. If not, do not establish any new long positions
* Gather stats for all symbols in S&P 500
	* SMA100
	* 90-day linear regression analysis
	* ATR
	* position weighting, based upon ATR and a risk parameter (e.g., 100 basis points)

After running the dashboard, decide:
* Take off any positions that 
	* drop from to 20% of S&P500, or
	* drop below their SMA100
* every other week
	* rebalance positions that are > |25%| from target ATR weight
* every week, if ^GSPC > SMA200
	* reinvest designated amount in new symbols according to ATR weight
	* reinvest until all funds fully invested