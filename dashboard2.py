import numpy as np
import pandas as pd
import pandas_datareader.data as pdr
import datetime
import statsmodels.api as sm
import urllib
import ConfigParser
from bs4 import BeautifulSoup
import MySQLdb as mdb
import pandas.io.sql as psql

"""
    Generates a table of statistics upon a set of ticker symbols, for example
    the S&P 500. Stack ranks symbols according to annualized, adjusted exponential
    regression slope in descending order. Provides indication of whether to trade
    based upon strategy outlined in Stocks On The Move book.
"""
def read_config():

    Config = ConfigParser.ConfigParser()
    Config.read("dashboard.config")
    print (Config.sections())
    return Config

def config_section_map(Config, section):
    dict1 = {}
    options = Config.options(section)
    for option in options:
        try:
            dict1[option] = Config.get(section, option)
            if dict1[option] == -1:
                print("skip: %s" % option)
        except:
            print("exception on %s!" % option)
            dict1[option] = None
    return dict1


def get_wiki_sp500_tickers():
    """
    scrape current S&P500 ticker symbols from the web and return list
    """
    symbols = []
    html = urllib.urlopen('https://en.wikipedia.org/wiki/List_of_S%26P_500_companies')

    page = html.read()
    soup = BeautifulSoup(page, "lxml")

    table = soup.find_all('table')[0]
    rows = table.find_all('tr')

    for tr in rows:
        tds = tr.find_all('td')
        for td in tds[0:1]:
            symbols.append(tds[0].get_text().encode("UTF-8"))

    return symbols

def get_yahoo_prices(ticker, start, end):
    def data(ticker):
        return pdr.DataReader(ticker, 'yahoo', start, end)
    datas = map(data, tickers)
    return pd.concat(datas, keys=tickers, names=['Ticker', 'Date'])

def get_db_sp500_prices(start, end, con):
    """
        Get all prices data for S&P symbols between start and end
        Ticker, Date, Open, High, Low, Close, Volume, Ad Close
    """
    sql = 'call spGetDailyPrices("' + str(start) + '", "' + str(end) + '")'
    return psql.read_sql(sql, con=con, index_col=['Ticker', 'Date'])

def print_results(output, file):
    """
    print symbols table in sorted desc order
    sort list and print out
    """
    print ("Printing output to " + file)
    output.to_csv(file)

if __name__ == "__main__":

    # get config data
    print ("Getting configuration data...")
    cfg = read_config()
    start = datetime.datetime.strptime(config_section_map(cfg, "Parameters")['start_date'], '%m/%d/%Y')
    end = datetime.datetime.strptime(config_section_map(cfg, "Parameters")['end_date'], '%m/%d/%Y')
    risk_factor = float(config_section_map(cfg, "Parameters")['risk_factor'])
    regression_period = int(config_section_map(cfg, "Parameters")['regression_period'])
    regression_125 = int(config_section_map(cfg, "Parameters")['regression_125'])
    regression_250 = int(config_section_map(cfg, "Parameters")['regression_250'])
    n = int(config_section_map(cfg, "Parameters")['atr_period'])
    account_value = float(config_section_map(cfg, "Parameters")['account_value'])
    output_file = config_section_map(cfg, "Parameters")['output_file']
    use_db = config_section_map(cfg,"Database")['use_database']
    db_host = config_section_map(cfg,"Database")['db_host']
    db_user = config_section_map(cfg,"Database")['db_user']
    db_pass = config_section_map(cfg,"Database")['db_pass']
    db_name = config_section_map(cfg,"Database")['db_name']

    if (use_db == "1"):
        print ("Getting price data from DB...")
        # Connect to the MySQL instance
        con = mdb.connect(db_host, db_user, db_pass, db_name)
        all_data = get_db_sp500_prices(start, end, con)

    else:
        print ("Getting S&P500 symbols from Wiki...")
        tickers = get_wiki_sp500_tickers()

        print ("Getting price data from Yahoo...")
        all_data = get_yahoo_prices(tickers, start, end)

    print ("Calculating indicators...")
    # calculate SMA200 for sp_500
    sp_500 = pdr.DataReader("^GSPC", "yahoo", start, end)
    SMA200 = np.mean(sp_500['Adj Close'][-200:])
    strRight = sp_500['Adj Close'][-1] > SMA200

    # calculate daily % move for all tickers data
    all_data["Pct_Change"] = all_data["Adj Close"].pct_change()
    all_data["Pct_Change"].fillna(0, inplace=True)

    # capture max daily % move for all tickers data
    just_pct_change = all_data[["Pct_Change"]].reset_index()

    daily_pct_cx = just_pct_change.pivot("Date", "Ticker", "Pct_Change")
    # Create/Add to output
    max_pct_change = pd.DataFrame(np.max(daily_pct_cx[-90:]), columns=['MaxPctChg'])

    # calculate SMA100 for last day of all tickers
    just_closing_prices = all_data[['Adj Close']].reset_index()
    daily_close_px = just_closing_prices.pivot('Date', 'Ticker','Adj Close')

    # add to DashboardOutput
    sma_prices = pd.DataFrame(np.mean(daily_close_px[-100:]), columns=['SMA100'])
    adj_close_prices = pd.DataFrame(np.max(daily_close_px[-1:]), columns=['Adj Close'])

    frames = (adj_close_prices, sma_prices, max_pct_change)
    output = pd.concat(frames, axis=1)
    output['ATR'] = ""      # Empty column for ATR

    tr_temp = all_data.copy()
    # drop unnecessary columns (there must be a better way to copy only desired columns)
    tr_temp.drop('Open', axis=1, inplace=True)
    tr_temp.drop('Adj Close', axis=1, inplace=True)
    tr_temp.drop('Volume', axis=1, inplace=True)

    # pivot tickers and high/low/close
    tr_temp.reset_index(inplace=True)

    #Collect unique tickers from dataframe
    ticker_list = pd.unique(tr_temp.Ticker.ravel())

    #tr_temp needs to have enough records to ensure ATR20 can be calculated for each of the last 20 trading days
    tr_temp = tr_temp.pivot('Date','Ticker')
    tr_temp = tr_temp[-41:]

    print ("Calculating 90-day linear regression values...")
    y = daily_close_px.iloc[-1*regression_period:].reset_index()  # gives entire matrix of adj close dates for symbols for last 90 days
    x = np.arange(0, regression_period)
    x = sm.add_constant(x)
    # add TR | Symbols columns to DF
    for s in ticker_list:
        true_range = pd.DataFrame( {"h-l":tr_temp['High',s] - tr_temp['Low', s],
                                   "h-cp":np.abs( tr_temp['High',s] - tr_temp['Close',s].shift(1)),
                                    "l-cp":np.abs( tr_temp['Low',s] - tr_temp['Close',s].shift(1))}, index=tr_temp.index)
        true_range = pd.Series( np.max( true_range.values, axis=1), index=tr_temp.index)
        # set TR columns
        tr_temp['TR', s] = true_range
        # n-period EMA makes this average true range (ATR)
	ATR = true_range.ewm(ignore_na=False, span=n, min_periods=n, adjust=True).mean()
        output.loc[s, 'ATR'] = ATR[-1:][0]
	# add Mean and STD for calculation of ATR z-score
	output.loc[s, 'MeanAtr'] = np.mean(ATR[-20:])
	output.loc[s, 'StdAtr'] = np.std(ATR[-20:]) 

        # Linear regression 90
        output.loc[s, 'M90'] = sm.OLS(np.log(y[s]), x).fit().params.x1
        output.loc[s, 'RSq90'] = sm.OLS(np.log(y[s]), x).fit().rsquared

    print ("Calculating 125-day linear regression values...")
    y = daily_close_px.iloc[-1*regression_125:].reset_index()  # gives entire matrix of adj close dates for symbols for last 125 days
    x = np.arange(0, regression_125)
    x = sm.add_constant(x)
    for s in ticker_list:
        output.loc[s, 'M125'] = sm.OLS(np.log(y[s]), x).fit().params.x1
        output.loc[s, 'RSq125'] = sm.OLS(np.log(y[s]), x).fit().rsquared

    print ("Calculating 250-day linear regression values...")
    y = daily_close_px.iloc[-1*regression_250:].reset_index()  # gives entire matrix of adj close dates for symbols for last 250 days
    x = np.arange(0, regression_250)
    x = sm.add_constant(x)
    for s in ticker_list:
        output.loc[s, 'M250'] = sm.OLS(np.log(y[s]), x).fit().params.x1
        output.loc[s, 'RSq250'] = sm.OLS(np.log(y[s]), x).fit().rsquared

    # Annual Slopes and Adjusted Slopes
    output['AnnualM90'] = np.power(1+output['M90'], 250) - 1
    output['AdjAnnualM90'] = np.multiply(output['AnnualM90'], output['RSq90'])

    output['AnnualM125'] = np.power(1+output['M125'], 250) - 1
    output['AdjAnnualM125'] = np.multiply(output['AnnualM125'], output['RSq125'])

    output['AnnualM250'] = np.power(1+output['M250'], 250) - 1
    output['AdjAnnualM250'] = np.multiply(output['AnnualM250'], output['RSq250'])

    # Calculate Trend (SMA100 > AdjClose)
    output['Trend'] = np.greater(output['Adj Close'], output['SMA100'])

    # Stock allocation based on volatility parity, weighted with relative z-score. Need to clip output to top 50 positions?
    print ("Calculating Stock Allocations...")
    # Calculate shares, Target% for all tickers
    # output['NumShares'] = np.floor_divide(np.multiply(account_value, risk_factor),output['ATR'])
    # output['RiskParityWeight'] = np.divide(np.multiply(output['NumShares'], output['Adj Close']), account_value)


    print ("Calculating Momentum Score and Sorting output...")
    output['MomentumScore'] = np.divide(np.add(output['AdjAnnualM125'], output['AdjAnnualM250']), 2)
    # sort output by Avg of AdjAnnualM125 and AdjAnnualM250
    output.sort_values(by='MomentumScore', ascending=False, inplace=True)

    print ("Dropping unwanted columns from output...")
    output.drop(['AnnualM90', 'M90', 'RSq90'], axis=1, inplace=True)
    output.drop(['M125', 'RSq125', 'M250', 'RSq250', 'AnnualM125', 'AnnualM250'], axis=1, inplace=True)
    
    print_results(output, output_file)
